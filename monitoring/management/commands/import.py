from django.core.management import BaseCommand
from django.utils.timezone import utc
import csv
import json
import re
import datetime
import itertools
from monitoring.models import Server, User, Session


def parseDateTime(string):
    for format in ("%Y-%m-%d %H:%M:%S.%f", "%y-%m-%d %H:%M:%S.%f"):
        try:
            return datetime.datetime.strptime(string, format).replace(tzinfo=utc)
        except ValueError:
            pass
    raise ValueError("Cannot parse time - " + string)

def parseDate(string):
    for year in ("Y", "y"):
        for p in itertools.permutations((year, "m", "d")):
            try:
                return datetime.datetime.strptime(string, "%{}-%{}-%{}".format(*p)).replace(tzinfo=utc)
            except ValueError:
                pass
    raise ValueError("Cannot parse date - " + string)

def standardiseLoginTime(string):
    return re.sub("[/\\\\|]", "-", string)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("csv_file", nargs='?', type=open, default="data/logins.csv")

    def handle(self, *args, **options):
        csv_file = options["csv_file"]
        csv_reader = csv.DictReader(csv_file, delimiter=",")

        for item in csv_reader:
            server = self.handle_server(item["server-ip"], item["server-name"])
            user = self.handle_user(item["username"], item["full-name"], item["contact"])
            login_time = standardiseLoginTime(item["login-time"])
            login_time_full = True
            try:
                login_time = parseDateTime(login_time)
            except ValueError:
                login_time = parseDate(login_time)
                login_time_full = False
            session = self.handle_session(server, user, login_time, login_time_full)

    @staticmethod
    def handle_server(server_ip, server_name):
        server = None
        try:
            server = Server.objects.filter(ip_address=server_ip).get()
        except Server.DoesNotExist:
            server = Server.objects.create(ip_address=server_ip)
        if not server.name and server_name:
            server.name = server_name
        server.save()
        return server

    @staticmethod
    def handle_user(username, full_name, contact):
        user = None
        try:
            user = User.objects.filter(username=username).get()
        except User.DoesNotExist as e:
            user = User.objects.create(username=username)
        if not user.full_name and full_name:
            user.full_name = full_name
        if contact:
            user.contact = user.contact + (contact,)
        user.save()
        return user

    @staticmethod
    def handle_session(server, user, login_time, login_time_full):
        try:
            session = Session.objects.filter(server=server, user=user).get()
            if not session.login_time_full and login_time_full:
                session.login_time = login_time
                session.login_time_full = login_time_full
        except Session.DoesNotExist:
            session = Session.objects.create(server=server, user=user, \
                login_time=login_time, login_time_full=login_time_full)
        session.save()
        return session
