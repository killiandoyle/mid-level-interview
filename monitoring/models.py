from django.db import models
import json


class User(models.Model):
    username = models.CharField(max_length=32, unique=True, db_index=True)
    full_name = models.CharField(max_length=128)
    _contact = models.CharField(max_length=4096, db_column="contact", default="[]")

    @property
    def contact(self):
        return tuple(json.loads(self._contact or '[]'))

    @contact.setter
    def contact(self, contact):
        self._contact = json.dumps(list(set(contact)))

class Server(models.Model):
    name = models.CharField(max_length=32, unique=True, blank=True, null=True)
    ip_address = models.GenericIPAddressField(unique=True, db_index=True)
    current_users = models.ManyToManyField(User, through="Session")

class Session(models.Model):
    login_time = models.DateTimeField()
    login_time_full = models.BooleanField(default=True)
    server = models.ForeignKey(Server, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        index_together = [
            ('server', 'user'),
        ]
