from django.shortcuts import render
from .models import Server


def logins(request):
    servers = Server.objects.order_by("name")
    return render(request, "../templates/logins.html", {'servers': servers})
