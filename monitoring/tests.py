from django.test import TestCase
from django.utils.timezone import utc
from models import User, Server, Session
import importlib
from datetime import datetime
from StringIO import StringIO


class UserTestCase(TestCase):
    def test_contact(self):
        user = User.objects.create(username="killian", full_name="Killian Doyle")
        self.assertCountEqual(user.contact, [])

        user.contact = ["killian@doyle.com"]
        self.assertEqual(user.contact, ("killian@doyle.com",))

        user.contact = user.contact + ("12345",)
        self.assertSetEqual(set(user.contact), set(("killian@doyle.com", "12345")))

class ImportTestCase(TestCase):
    imp = importlib.import_module("monitoring.management.commands.import")
    Command = imp.Command

    def test_parse_date(self):
        self.assertEquals(self.imp.parseDate("2006-12-01"), datetime(2006, 12, 1) \
            .replace(tzinfo=utc))
        self.assertEquals(self.imp.parseDate("22-12-2006"), datetime(2006, 12, 22) \
            .replace(tzinfo=utc))

    def test_import_sample_data(self):
        command = self.Command()
        csv_text = """server-name,server-ip,username,full-name,contact,login-time
,201.23.18.80,dave,Leon Green,12345,2017/06/19
buckley,93.25.137.164,colinwatson,Ian Hughes,kerry78@carter-hunt.com,2017-06-19 13:37:16.606810
stone,201.23.18.80,dave,Leon Green,a@b.c,2017-06-19 13:37:16.606449

"""
        csv_file = StringIO(csv_text)
        csv_file.seek(0)
        command.handle(csv_file=csv_file)
        server = Server.objects.filter(ip_address="201.23.18.80").get()
        self.assertEquals(server.name, "stone")
        user = User.objects.filter(username="dave").get()
        self.assertEquals(user.username, "dave")
        self.assertEquals(set(user.contact), set(("12345", "a@b.c")))
        session = Session.objects.filter(server=server, user=user).get()
        self.assertEquals(session.login_time, datetime(2017, 6, 19, 13, 37, 16, 606449).replace(tzinfo=utc))

    def test_blank_server_name(self):
        command = self.Command()
        command.handle_server("1.1.1.1", "")
        command.handle_server("1.1.1.1", "name")
        command.handle_server("1.1.1.1", "")
        server = Server.objects.filter(ip_address="1.1.1.1").get()
        self.assertEquals(server.name, "name")
    
    def test_user_contact(self):
        command = self.Command()
        command.handle_user("username", "Full Name", "a@b.c")
        command.handle_user("username", "Full Name", "1234")
        user = User.objects.filter(username="username").get()
        self.assertEquals(set(user.contact), set(("a@b.c", "1234")))
    
    def test_session_times(self):
        command = self.Command()
        server = Server.objects.create(ip_address="1.1.1.2", name="name")
        user = User.objects.create(username="me")
        server.save()
        user.save()

        dt = datetime(2000, 1, 1).replace(tzinfo=utc)
        command.handle_session(server, user, dt, False)
        session = Session.objects.filter(server=server, user=user).get()
        self.assertEquals(session.login_time, dt)
        self.assertEquals(session.login_time_full, False)

        dt = datetime(2000, 1, 1, 1, 1, 1).replace(tzinfo=utc)
        session = command.handle_session(server, user, dt, True)
        session = Session.objects.filter(server=server, user=user).get()
        self.assertEquals(session.login_time_full, True)
        self.assertEquals(session.login_time, dt)
        