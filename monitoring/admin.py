from django.contrib import admin
import models


admin.site.register([models.Server, models.Session, models.User])
